const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose
  .connect(
    "mongodb+srv://twapegg:mEDXlo13wjUGhSal@wdc028-course-booking.5akuxcl.mongodb.net/",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      dbName: "s37-41API",
    }
  )
  .then(() => {
    console.log("Connected to MongoDB database.");
  })
  .catch((err) => {
    console.log("Error connecting to MongoDB database.", err);
  });

app.use("/users", require("./routes/userRoutes"));
app.use("/courses", require("./routes/courseRoutes"));

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.listen(process.env.PORT || 3000, () => {
  console.log("API listening on port 3000!");
});
