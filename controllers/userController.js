const bcrypt = require("bcrypt");
const auth = require("../auth");

const User = require("../models/user");
const Course = require("../models/course");

const getAllUsers = () => {
  return User.find().then((result) => {
    return result;
  });
};

const checkEmailExists = (req) => {
  return User.find({
    email: req.email,
  }).then((result) => {
    // check if user exists
    return result.length > 0;
  });
};

const registerUser = (req) => {
  // check if mail already exists
  return User.create({
    firstName: req.firstName,
    lastName: req.lastName,
    email: req.email,
    password: bcrypt.hashSync(req.password, 10),
    mobileNo: req.mobileNo,
  }).then((result) => {
    return result;
  });
};

const loginUser = (req) => {
  return User.findOne({
    email: req.email,
  }).then((user) => {
    if (user) {
      const isPasswordCorrect = bcrypt.compareSync(req.password, user.password);
      // Generate access token
      return isPasswordCorrect
        ? { access: auth.createAccessToken(user) }
        : false;
    }
    return false;
  });
};

const getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    result.password = "";
    return result;
  });
};

const enrollUser = async (data) => {
  let isUserUpdated = await User.findById(data.userId).then((result) => {
    result.enrollments.push({ courseId: data.courseId });

    return result.save().then((result) => {
      return true;
    });
  });

  let isCourseUpdated = await Course.findById(data.courseId).then((result) => {
    result.enrollees.push({ userId: data.userId });

    return result.save().then((result) => {
      return true;
    });
  });

  if (isUserUpdated && isCourseUpdated) {
    return true;
  }
};

module.exports = {
  checkEmailExists,
  registerUser,
  loginUser,
  getProfile,
  getAllUsers,
  enrollUser,
};
