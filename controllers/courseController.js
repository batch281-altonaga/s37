const Course = require("../models/course");

// Create course, only admins can create
const addCourse = (req) => {
  const course = new Course(req);
  return course.save().then((result) => {
    return result;
  });
};

// Retrieve all courses
const getAllCourses = () => {
  return Course.find().then((courses) => {
    return courses;
  });
};

// Retrieve all active courses
const getAllActiveCourses = () => {
  return Course.find({ isActive: true }).then((courses) => {
    return courses;
  });
};

// Retrieve specific course
const getCourse = (id) => {
  return Course.findById(id).then((course) => {
    return course;
  });
};

// Update course
const updateCourse = (id, req) => {
  return Course.findByIdAndUpdate(id, req).then((result) => {
    return result;
  });
};

// // Delete a course
// const deleteCourse = (id) => {
//   return Course.findByIdAndDelete(id).then((result) => {
//     return result;
//   });
// };

// s40 activity

// Archive a course (admin only)
const archiveCourse = (id) => {
  return Course.findByIdAndUpdate(id, { isActive: false }).then((result) => {
    return result;
  });
};

module.exports = {
  addCourse,
  getAllCourses,
  getAllActiveCourses,
  getCourse,
  updateCourse,
  // deleteCourse,
  archiveCourse,
};
