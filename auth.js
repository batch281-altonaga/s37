const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// JSON WEB TOKENS

module.exports.createAccessToken = (user) => {
  const payload = {
    user: {
      id: user._id,
      email: user.email,
      isAdmin: user.isAdmin,
    },
  };

  return jwt.sign(payload, secret, { expiresIn: "1h" });
};

// Token verification
module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;

  if (typeof token !== "undefined") {
    console.log(token);

    token = token.slice(7, token.length);

    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return res.send({ auth: "failed" });
      } else {
        next();
      }
    });
  } else {
    return res.send({ auth: "failed" });
  }
};

// Token Decryption
module.exports.decode = (token) => {
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);

    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, { complete: true }).payload.user;
      }
    });
  } else {
    return null;
  }
};
