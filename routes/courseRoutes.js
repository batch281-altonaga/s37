const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Create course, only admins can create
router.post("/", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    courseController.addCourse(req.body).then((result) => {
      res.send(result);
    });
  } else {
    res.send({ auth: "failed" });
  }
});

// Retrieve all courses
router.get("/all", (req, res) => {
  courseController.getAllCourses().then((result) => {
    res.send(result);
  });
});

// Retrieve all active courses
router.get("/", (req, res) => {
  courseController.getAllActiveCourses().then((result) => {
    res.send(result);
  });
});

// Retrieve specific course
router.get("/:id", (req, res) => {
  courseController.getCourse(req.params.id).then((result) => {
    res.send(result);
  });
});

// Update course
router.put("/:id", auth.verify, (req, res) => {
  courseController.updateCourse(req.params.id, req.body).then((result) => {
    res.send(result);
  });
});

// Delete a course
// router.delete("/:id", auth.verify, (req, res) => {
//   const userData = auth.decode(req.headers.authorization);

//   if (userData.isAdmin) {
//     courseController.deleteCourse(req.params.id).then((result) => {
//       res.send(result);
//     });
//   } else {
//     res.send({ auth: "failed" });
//   }
// });

// s40 activity

// Archive a course (admin only)
router.patch("/:id", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    courseController.archiveCourse(req.params.id).then((result) => {
      res.send(result);
    });
  } else {
    res.send({ auth: "failed" });
  }
});

module.exports = router;
